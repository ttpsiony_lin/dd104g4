// fetch API
//
// fetch('url', {
//   method: 'POST',
//   headers: {
//     Accept: 'application/json',
//     'Content-Type': 'application/json'
//   },
//   body: JSON.stringify(json)
// })
//   .then(res => res.json())
//   .then(json => console.log(json))
//   .catch(err => console.log(err))
//

//
//
//
let allActNo = []
let curAct_NO

//活動審核頁面
const allActivityTemp = item => {
  str = `
    <div class="col-md-4 border border-primary" style="margin: 30px 0px">
      <!--  -->
      <div class="row">
        <div class="col-md-6">
            <div class="h6">活動編號 :
                <span>${item[0]}</span>
            </div>
            <div class="h6">會員編號 :
                <span>${item[1]}</span>
            </div>
            <div class="h6">活動名稱 :
                <span>${item[2]}</span>
            </div>
            <div class="h6">人數上限 : <span>${item[7]}</span></div>
            <div class="h6">
              <label>
                  活動日期 <input class="mt-2" type="date" value="${
                    item[3] ? item[3] : ''
                  }">
              </label>
            </div>
            <div class="h6">
              <label class="btn btn-outline-primary border border-primary p-2 rounded mt-2"
                  style=" cursor:pointer">
                  選擇海報
                  <input class="uploadPosterBtn" type="file" hidden name="uploadPoster_input">
              </label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="h6 d-flex justify-content-between" style="margin-bottom:40px">
                <span>活動圖片</span>
                <img style="height:150px;" src="./php/act/${item[6]}">
            </div>
            <div class="h6 d-flex justify-content-between">
                <span>活動海報</span>
                <img style="height:150px" src="./php/act/${
                  item[8] ? item[8] : 'est_poster/muscle.png'
                }" data-postername="${
    item[8] ? item[8].split('/')[item[8].split('/').length - 1] : 'muscle.png'
  }">
            </div>
        </div>
      </div>
      <!--  -->
      <div class="h6 mt-4">活動介紹 : <br>
        <span>${item[4]}</span>
      </div>
      <!--  -->
      <div class="h6">狀態 :
        <!-- 下拉選單 -->
        <div class="btn-group">
            <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false" ${
                  item[5] == 2 ? 'disabled' : ''
                } >
                ${item[5] == 0 ? '待審核' : item[5] == 1 || 3 ? '上架' : '下架'}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <button class="dropdown-item actChangeBtn" type="button" data-state="1">上架</button>
                <!-- 
                <button class="dropdown-item actChangeBtn" type="button" data-state="2">下架</button>
                -->
                <button class="dropdown-item actChangeBtn" type="button" data-state="0">待審核</button>
            </div>
        </div>
      </div>
    </div>
    `
  return str
}

const changeActState = e => {
  const outerDiv = e.target.parentNode.parentNode.parentNode.parentNode
  const changeState = e.target.dataset.state
  const actNum =
    outerDiv.children[0].children[0].children[0].children[0].innerText

  let date =
    outerDiv.children[0].children[0].children[4].children[0].children[0].value
  let poster =
    outerDiv.children[0].children[1].children[1].children[1].dataset.postername

  // 當期活動不能修改
  if (curAct_NO === actNum) {
    Swal.fire({
      html: '<div style="font-size:18px">請先更換當期活動才能修改</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  // changeState = 0 or 1
  if (changeState === '1') {
    // 確認日期
    if (date === '') {
      Swal.fire({
        html: '<div style="font-size:18px">活動日期還沒填!!!</div>',
        confirmButtonText: '確定',
        confirmButtonColor: '#f46f65',
        background: '#fff url("./img/p0080_m.jpg") center'
      })
      return
    }
    // 確認上傳海報
    if (poster === 'muscle.png') {
      Swal.fire({
        html: '<div style="font-size:18px">活動海報還沒上傳喔!!!</div>',
        confirmButtonText: '確定',
        confirmButtonColor: '#f46f65',
        background: '#fff url("./img/p0080_m.jpg") center'
      })
      return
    }
    //確認後送出
    changeActStateAJAX(changeState, actNum, date, poster)

    Swal.fire({
      html: '<div style="font-size:18px">更新完成!!!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(() => {
      location.reload()
    })

    //
  } else {
    //
    Swal.fire({
      html: '<div style="font-size:18px">更新完成!!!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(() => {
      location.reload()
    })
    changeActStateAJAX(changeState, actNum)
  }

  // 改變顯示狀態 (上下架或審核)
  e.target.parentNode.parentNode.children[0].innerText = e.target.innerText
}

const changeActStateAJAX = (state, actNum, date = 'none', poster = 'none') => {
  $.ajax({
    url: './php/act/back-end_activity.php',
    type: 'post',
    dataType: 'json',
    data: {
      isChange: 1,
      state: state,
      actNum: actNum,
      date: date,
      poster: poster
    },
    success: function(data) {
      Swal.fire({
        html: '<div style="font-size:18px">更新完成!!!</div>',
        confirmButtonText: '確定',
        confirmButtonColor: '#f46f65',
        background: '#fff url("./img/p0080_m.jpg") center'
      }).then(() => {
        location.reload()
      })
    },
    error: function(data) {
      console.log('error')
    }
  })
}

const changePoster = e => {
  const row = e.target.parentNode.parentNode.parentNode.parentNode
  const img = row.children[1].children[1].children[1]
  img.dataset.postername = e.target.files[0].name

  if (e.target.files[0]) {
    let reader = new FileReader()
    reader.readAsDataURL(e.target.files[0])
    reader.onload = function(e) {
      img.src = e.target.result
    }
  }
}

//
//
//
// 參賽報名審核
const allRegistrationCompetitor = (item, index = -1) => {
  let tr = document.createElement('tr')
  if (index !== -1) {
    tr.className = 'hoverEffect'
    let len = item.length - 1
    for (let i = 0; i < len; i++) {
      let td = document.createElement('td')
      td.className = 'text-center h6'
      if (i === 4) {
        let select = document.createElement('select')
        select.className = 'btn btn-outline-primary'
        for (let j = 0; j < 2; j++) {
          let option = document.createElement('option')
          option.className = 'btn btn-primary'
          option.value = j
          option.innerText = j == 0 ? '未過' : '通過'
          if (item[i] == j) option.selected = 'selected'
          select.appendChild(option)
        }
        if (item[5] == 2) select.disabled = 'disabled'
        td.appendChild(select)
        select.addEventListener('change', changeCompetitorState)
      } else if (i === 2) {
        td.innerHTML = `<img style="height:200px" src="./php/act/${item[i]}"/>`
      } else if (i === 1) {
        td.innerText = item[i]
        td.style.position = 'relative'
        let pic = new Image()
        pic.src = `./img/act/${index + 1}.png`
        pic.style.height = '100px'
        pic.style.position = 'absolute'
        pic.style.right = '30px'
        pic.zIndex = '5'
        td.appendChild(pic)
      } else {
        td.innerText = item[i]
      }
      tr.appendChild(td)
    }
    // status = 2
  } else {
    tr.className = 'hoverEffect'
    let len = item.length - 1
    for (let i = 0; i < len; i++) {
      let td = document.createElement('td')
      td.className = 'text-center h6'
      if (i === 4) {
        let select = document.createElement('select')
        select.className = 'btn btn-outline-primary'
        for (let j = 0; j < 2; j++) {
          let option = document.createElement('option')
          option.className = 'btn btn-primary'
          option.value = j
          option.innerText = j == 0 ? '未過' : '通過'
          if (item[i] == j) option.selected = 'selected'
          select.appendChild(option)
        }
        if (item[5] == 2) select.disabled = 'disabled'
        td.appendChild(select)
        select.addEventListener('change', changeCompetitorState)
      } else if (i === 2) {
        td.innerHTML = `<img style="height:200px" src="./php/act/${item[i]}"/>`
      } else {
        td.innerText = item[i]
      }
      tr.appendChild(td)
    }
    // status != 2
  }
  return tr
}

const changeCompetitorState = e => {
  const activity_NO = e.target.parentNode.parentNode.children[0].innerText

  if (curAct_NO === activity_NO) {
    Swal.fire({
      html: '<div style="font-size:18px">當期活動不能更動</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      location.reload()
    })
  } else {
    const mem_NO = e.target.parentNode.parentNode.children[1].innerText
    $.ajax({
      url: './php/act/back-end_registration.php',
      type: 'post',
      dataType: 'json',
      data: { isChange: 1, mem_NO: mem_NO, registration_State: e.target.value },
      success: function(data) {
        Swal.fire({
          html: '<div style="font-size:18px">更新完成!!!</div>',
          confirmButtonText: '確定',
          confirmButtonColor: '#f46f65',
          background: '#fff url("./img/p0080_m.jpg") center'
        })
      },
      error: function(data) {
        console.log('error')
      }
    })
  }
}

//
//
//
// 留言檢舉
const someCommentsProblem = item => {
  let tr = document.createElement('tr')
  tr.className = 'hoverEffect'
  for (let i = 0; i < item.length; i++) {
    let td = document.createElement('td')
    td.className = 'text-center h6'
    if (i == 3) {
      let select = document.createElement('select')
      select.className = 'btn btn-outline-primary'
      for (let j = 0; j < 3; j++) {
        let option = document.createElement('option')
        option.className = 'btn btn-primary'
        option.value = j
        option.innerText =
          j == 0 ? '隱藏留言' : j == 1 ? '正常留言' : '被檢舉留言'
        select.appendChild(option)
      }
      if (item[i] == 0) select.firstChild.selected = 'selected'
      if (item[i] == 2) select.lastChild.selected = 'selected'
      td.appendChild(select)
      select.addEventListener('change', changeCommentState)
    } else {
      td.innerText = item[i]
    }
    tr.appendChild(td)
  }
  return tr
}

const changeCommentState = e => {
  const comment_NO = e.target.parentNode.parentNode.children[0].innerText
  $.ajax({
    url: './php/act/back-end_comment.php',
    type: 'post',
    dataType: 'json',
    data: {
      isChange: 1,
      comment_NO: comment_NO,
      comment_state: e.target.value
    },
    success: function(data) {
      Swal.fire({
        html: '<div style="font-size:18px">更新完成!!!</div>',
        confirmButtonText: '確定',
        confirmButtonColor: '#f46f65',
        background: '#fff url("./img/p0080_m.jpg") center'
      }).then(result => {
        location.reload()
      })
    },
    error: function(data) {
      console.log('error')
    }
  })
}

//
//改變當期活動
const confirmCurAct = () => {
  let curAct = document.querySelector('.curAct')

  Swal.fire({
    html: `<div style="font-size:18px">確認更改當期活動，編號${curAct.value}</div>`,
    confirmButtonText: '確定',
    confirmButtonColor: '#f46f65',
    showCancelButton: true,
    cancelButtonColor: '#aaa',
    cancelButtonText: '取消',
    background: '#fff url("./img/p0080_m.jpg") center'
  }).then(result => {
    if (result.value) {
      let findOut = allActNo.find(item => item === curAct.value)
      if (findOut) {
        $.ajax({
          url: './php/act/back-end_activity.php',
          type: 'post',
          dataType: 'json',
          data: { isChange: 2, actNum: curAct.value, overdueActNO: +curAct_NO },
          success: function(data) {
            location.reload()
            // console.log(data)
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        Swal.fire({
          html: `<div style="font-size:18px">找不到該活動編號!!!</div>`,
          confirmButtonText: '確定',
          confirmButtonColor: '#f46f65',
          background: '#fff url("./img/p0080_m.jpg") center'
        })
      }
    }
  })
}

// AJAX
//
//
//
//
//
//
//
const href = location.href.split('/')[location.href.split('/').length - 1]
if (href === 'backgroundActivity.html') {
  //
  //活動
  $.ajax({
    url: './php/act/back-end_activity.php',
    type: 'post',
    dataType: 'json',
    data: { isChange: 0 },
    success: function(data) {
      // console.log(data)
      let allActStr = ''
      data.forEach(item => {
        let a = Object.values(item)
        allActStr += allActivityTemp(a)
        allActNo.push(a[0])
        if (a[5] === '3') {
          let curAct = document.querySelector('.curAct')
          curAct.value = a[0]
          curAct_NO = a[0]
        }
      })
      let actContainer = document.querySelector('#actContainer')
      actContainer.innerHTML = allActStr
      let actChangeBtn = document.querySelectorAll('.actChangeBtn')
      actChangeBtn.forEach(item =>
        item.addEventListener('click', changeActState)
      )
      let uploadPosterBtn = document.querySelectorAll('.uploadPosterBtn')
      uploadPosterBtn.forEach(item =>
        item.addEventListener('change', changePoster)
      )
    },
    error: function(data) {
      console.log(data)
    }
  })

  let changeActBtn = document.querySelector('.changeActBtn')
  changeActBtn.addEventListener('click', confirmCurAct)
  //
  //
} else if (href === 'backgroundActivity-2.html') {
  //
  //參賽
  $.ajax({
    url: './php/act/back-end_registration.php',
    type: 'post',
    dataType: 'json',
    data: { isChange: 0 },
    success: function(data) {
      curAct_NO = [
        ...new Set(data.filter(item => item.activity_State === '3'))
      ][0].activity_NO
      document.querySelector('.curActNOspan').innerText = curAct_NO

      // 下架與未下架活動區分
      let bbb = data.filter(item => item.activity_State !== '2')
      bbb.forEach(item => {
        let a = Object.values(item)
        let tr = allRegistrationCompetitor(a)
        let table = document.querySelector('.table')
        table.appendChild(tr)
      })

      let overdueAct = []
      data
        .filter(item => item.activity_State === '2')
        .forEach(item => overdueAct.push(item.activity_NO))
      overdueAct = [...new Set(overdueAct)]

      overdueAct.forEach(actNO => {
        let aaa = data.filter(item => item.activity_NO === actNO)
        aaa.forEach((item, index) => {
          if (index <= 2) {
            let a = Object.values(item)
            let tr = allRegistrationCompetitor(a, index)
            let table = document.querySelector('.table')
            table.appendChild(tr)
          } else {
            let a = Object.values(item)
            let tr = allRegistrationCompetitor(a)
            let table = document.querySelector('.table')
            table.appendChild(tr)
          }
        })
      })
    },
    error: function(data) {
      console.log(data)
    }
  })
  //
  //留言
} else if (href === 'backgroundReport.html') {
  //
  //留言
  $.ajax({
    url: './php/act/back-end_comment.php',
    type: 'post',
    dataType: 'json',
    data: { isChange: 0 },
    success: function(data) {
      // console.log(data)
      if (data) {
        data.forEach(item => {
          let a = Object.values(item)
          let tr = someCommentsProblem(a)
          let table = document.querySelector('.table')
          table.appendChild(tr)
        })
      }
    },
    error: function(data) {
      console.log(data)
    }
  })
  //
  //
}
