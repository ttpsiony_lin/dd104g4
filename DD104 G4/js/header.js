// 公用js
window.addEventListener('load', function() {
  $('.hamburger').click(function() {
    $(this).toggleClass('is-opened')
    $('.header_phone').slideToggle(1500)
  })

  //撈sesstion資料

  $.ajax({
    type: 'GET',
    url: './php/header.php',
    dataType: 'json', //接收到的資料型態
    crossDomain: true,
    success: function(data) {
      console.log(data['mem_Email'])
      if (data['mem_Email']) {
        //登入成功
        $('#memout,#memout_m').html('登出')
        $('.iconmember').css({
          'background-color': '#F8C12D',
          'border-radius': '70%'
        })
        // 限制為登入狀況下進入會員
        $('#Noentry,#Noentry_m').attr('href', 'member.html')
      } else if (data['mem_Email'] == undefined) {
        $('#Noentry,#Noentry_m')
          .removeAttr('href')
          .css({
            cursor: 'pointer'
          })
          .click(function() {
            Swal.fire({
              title: '尚未登入',
              html:
                '<br><p>請<a href ="login.html" style=font-size:25px;outline:none;text-decoration:none;>註冊、登入</a>會員，才能使用</p>',
              showConfirmButton: false,
              background: '#fff url("./img/p0080_m.jpg")'
            })
          })
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(xhr.status)
      alert(thrownError)
      alert(ajaxOptions)
    }
  })

  $('#memout,#memout_m').click(function() {
    if ($('#memout,#memout_m').html() == '登入') {
      window.location.href = 'login.html'
    } else if ($('#memout,#memout_m').html() == '登出') {
      $.ajax({
        type: 'GET',
        url: './php/logout.php',
        dataType: 'text', //接收到的資料型態
        crossDomain: true,
        success: function(data) {
          if (data) {
             window.location.href = 'home.html';
            $('#memout,#memout_m').html('登入')
            // window.location.replace("home.html");
           
          }
        }
        ,
        error: function(xhr, ajaxOptions, thrownError) {
          alert(xhr.status)
          alert(thrownError)
          alert(ajaxOptions)
        }
      })
    }
  })
})
