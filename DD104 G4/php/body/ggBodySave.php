<?php
// $_GET["mem_NO"]
$errMsg = "";
try{
    require_once("../connectBook.php");

    $sql = "insert into `history_menu` (mem_NO,historyMenu_Date) values(?,CURRENT_TIME)";
    $menu = $pdo->prepare($sql);
    $menu->bindValue(1, $_GET["mem_NO"]);
    $menu->execute();

    $sql2 = "SELECT `historyMenu_NO` FROM `history_menu` ORDER BY historyMenu_Date DESC ";
    $time = $pdo->query($sql2);
   $row = $time->fetch();
   echo json_encode($row["historyMenu_NO"]);
    
}catch(PDOException $e){
    $errMsg .= "錯誤原因 : ".$e -> getMessage(). "<br>";
    $errMsg .= "錯誤行號 : ".$e -> getLine(). "<br>";
  }
?>
