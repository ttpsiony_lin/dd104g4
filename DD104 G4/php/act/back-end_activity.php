<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");


if ($_POST["isChange"] == 0) {
    $sql = "select * from `activity`";
    $activity = $pdo->query($sql);

    while ($Row = $activity->fetchObject()) {
        $actData[] = $Row;
    }

    echo json_encode($actData);
    // 
} else if ($_POST["isChange"] == 1) {

    if ($_POST["date"] == "none" || $_POST["poster"] == "none") {
        $sql = "update `activity` set activity_State = :state, activity_Date = NULL, activity_Poster = NULL where activity_NO = :actNum";
        $activity = $pdo->prepare($sql);
        $activity->bindValue(":state", $_POST["state"]);
        $activity->bindValue(":actNum", $_POST["actNum"]);

        $activity->execute();
    } else {
        $sql = "update `activity` set activity_State = :state, activity_Date = :date, activity_Poster = :poster where activity_NO = :actNum";
        $activity = $pdo->prepare($sql);
        $activity->bindValue(":state", $_POST["state"]);
        $activity->bindValue(":actNum", $_POST["actNum"]);
        $activity->bindValue(":date", $_POST["date"]);
        $activity->bindValue(":poster", "est_poster/" . $_POST["poster"]);

        $activity->execute();
    }

    echo json_encode(["status" => "ok"]);
} else {

    // 取全部資料
    $sql = "select registration_NO, mem_NO, votes from `registration` where activity_NO = :overdueActNO and registration_State = 1 order by votes desc";

    $coupon = $pdo->prepare($sql);
    $coupon->bindValue(":overdueActNO", $_POST["overdueActNO"]);
    $coupon->execute();


    for ($i = 0; $i < 3; $i++) {
        $Row = $coupon->fetchObject();
        $dataArr[] = [$Row->mem_NO  => (int) $Row->votes];
        $sql = "INSERT INTO `member_coupon` VALUES (NULL, :couponNO, :mem_NO, 0)";
        $a = $pdo->prepare($sql);
        $a->bindValue(":couponNO", $i + 1);
        $a->bindValue(":mem_NO", $Row->mem_NO);
        $a->execute();
    }


    // 改活動狀態
    $sql = "UPDATE `activity` SET activity_State = ( CASE 
    when activity_NO = :actNum then 3 
    when activity_NO != :actNum and activity_State = 3 then 2 
    when activity_NO != :actNum and activity_State = 2 then 2 
    when activity_NO != :actNum and activity_State != 3 and activity_State != 2 then 1 END ) 
    WHERE activity_State != 0";
    $a = $pdo->prepare($sql);
    $a->bindValue(":actNum", $_POST["actNum"]);
    $a->execute();

    echo json_encode(["status" => "ok"]);
}
