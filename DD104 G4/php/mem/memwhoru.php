<?php
try{

require_once("../connectBook.php");

$sql = "select * from `member` where `mem_NO` = :mem_NO ";
$memback = $pdo->prepare($sql);
$memback->bindValue(":mem_NO", $_GET['mem_NO']);
$memback->execute();
$membackRow = $memback->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($membackRow);

}catch(PDOException $e){
    echo $e->getMessage();
}
?>
