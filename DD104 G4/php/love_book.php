<?php
try {
    require_once("connectBook.php");

    $isChangeFavorite = json_decode(file_get_contents("php://input"))->isChangeFavorite;

    if ($isChangeFavorite == 1) {
        // change the member favorite book status
        $sql = "select * from `save_article` where mem_NO = :mem_NO and knowledge_NO = :knowledge_NO";
        $checkFavorite = $pdo->prepare($sql);
        $data = json_decode(file_get_contents("php://input"));
        $checkFavorite->bindValue(":mem_NO", $data->mem_NO);
        $checkFavorite->bindValue(":knowledge_NO", $data->knowledge_NO);
        $checkFavorite->execute();

        if ($checkFavorite->rowCount() != 0) {
            $sql = "update `save_article` set my_Article = :my_Article where mem_NO = :mem_NO and knowledge_NO = :knowledge_NO";
            $favorite = $pdo->prepare($sql);

            $favorite->bindValue(":mem_NO", $data->mem_NO);
            $favorite->bindValue(":knowledge_NO", $data->knowledge_NO);
            // echo json_encode(array('a' =>  $data->mem_NO, "b" =>  $data->knowledge_NO));


            // echo json_encode(array("a" => $data->my_Article));
            if ($data->my_Article == 1) {
                $favorite->bindValue(":my_Article", $data->my_Article);
                $favorite->execute();
                echo json_encode(array("my_Article" => 0));
            } else {
                $favorite->bindValue(":my_Article", $data->my_Article);
                $favorite->execute();
                echo json_encode(array("my_Article" => 1));
            }
        } else {
            $sql = "INSERT INTO `save_article` VALUE (NULL, :mem_NO, 1, :knowledge_NO)";
            $aaa = $pdo->prepare($sql);
            $aaa->bindValue(":mem_NO", $data->mem_NO);
            $aaa->bindValue(":knowledge_NO", $data->knowledge_NO);
            $aaa->execute();
            echo json_encode(array("my_Article" => 0));
        }

        // 
    } else {
        // loading for which the member favorite books are
        $sql = 'select knowledge_NO from `save_article` where mem_NO = :mem_NO and my_Article = 1 order by knowledge_NO';
        $favoriteBooks = $pdo->prepare($sql);
        $favoriteBooks->bindValue(":mem_NO", json_decode(file_get_contents("php://input"))->mem_NO);
        $favoriteBooks->execute();

        if ($favoriteBooks->rowCount()) {
            while ($Row = $favoriteBooks->fetchObject()) {
                $data[] = $Row->knowledge_NO;
            }
            echo json_encode($data);
        } else {
            echo json_encode([]);
        }
    }
} catch (PDOException $e) {
    echo "例外行號 : ", $e->getLine(), "<br>";
    echo "例外原因 : ", $e->getMessage(), "<br>";
}
