function doFirst(){
    getmemdata();
   
    var btnsearch =document.getElementById("btnsearch");
    btnsearch.addEventListener("click",function(){
        var memsearch = document.querySelector(".form-control").value;
      
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
        if (xhr.status == 200) {
            showwhorumemdata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = `./php/mem/memwhoru.php?mem_NO=${memsearch}`;
    xhr.open("Get", url, true);
    xhr.send(null);
       
    })
}
function showwhorumemdata(urjson){
    let urjsoneData = JSON.parse(urjson);
    var memallmemA = document.getElementById("showsearchmem");
    let htmlmemallmemA = "";
        htmlmemallmemA += `
       
        <thead>
        <tr>
        <th class="text-center  h5">會員編號</th>
        <th class="text-center  h5">帳號(信箱)</th>
        <th class="text-center  h5">姓名</th>
        <th class="text-center  h5">停權</th>
        </tr>
        </thead>

        <tbody>
        <tr>
        <td class="text-center">${urjsoneData[0]['mem_NO']}</td>
        <td class="text-center">${urjsoneData[0]['mem_Email']}</td>
        <td class="text-center">${urjsoneData[0]['mem_Name']}</td>
        <td class="text-center">
        <select class="custom-select mr-sm-2">`

            if(urjsoneData[0]['mem_State']==1){
                htmlmemallmemA += `
                <option selected value="1">正常</option>
                <option value="0">停權</option>`
            }else{
                htmlmemallmemA += `
                <option value="1">正常</option>
                <option selected value="0">停權</option>`
            }
                
        htmlmemallmemA += 
         `
        </select>
        </td>

        <td class="text-center">
        <button class="btn btn-primary btneditmemA">確認修改<button>
        </td>
        </tr>
        </tbody>

        `;
  
    memallmemA.innerHTML = htmlmemallmemA;

    var btneditmemA = document.querySelector(".btneditmemA");
    btneditmemA.addEventListener("click",function(e){
        // console.log(e.target.parentElement.previousElementSibling.firstElementChild.value);

        var memState = e.target.parentElement.previousElementSibling.firstElementChild.value;
        var memNO = e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML;
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            if (xhr.status == 200) {
            //    alert("修改成功");
                Swal.fire({
                    html: '<div style="font-size:18px">修改成功!!!</div>',
                    confirmButtonText: '確定',
                    confirmButtonColor: '#f46f65',
                    background: '#fff url("./img/p0080_m.jpg") center'
                }).then(result => {
                    if (result.value) {
                        location.reload();
                    }
                })
            //    location.reload();
            } else {
                alert(xhr.status);
            }
        }
    
        var url = `./php/mem/membackaupdatemem.php?mem_State=${memState}&mem_NO=${memNO}`;
        xhr.open("Get", url, true);
        xhr.send(null);

    })

}

//抓全部會員近來
function showmemdata(jsonmem){
    let jsonmemeData = JSON.parse(jsonmem);
    var memallmem = document.getElementById("memallmem");
    let htmlmemallmem = "";
    for (var i in jsonmemeData) {
       
        htmlmemallmem += `
        <tr>
        <td class="text-center">${jsonmemeData[i]['mem_NO']}</td>
        <td class="text-center">${jsonmemeData[i]['mem_Email']}</td>
        <td class="text-center">${jsonmemeData[i]['mem_Name']}</td>
        <td class="text-center">
            <select class="custom-select mr-sm-2">`

            if(jsonmemeData[i]['mem_State']==1){
                htmlmemallmem += `
                <option selected value="1">正常</option>
                <option value="0">停權</option>`
            }else{
                htmlmemallmem += `
                <option value="1">正常</option>
                <option selected value="0">停權</option>`
            }
                
        htmlmemallmem += 
            `</select>
        </td>
        <td class="text-center">
        <button class="btn btn-primary btneditmem">確認修改<button>
        </td>
        </tr>
        `;

        
    }
    memallmem.innerHTML = htmlmemallmem;
    var btneditmem = document.querySelectorAll(".btneditmem");
    for(var p =0 ; p<btneditmem.length;p++){
          btneditmem[p].addEventListener("click",function(e){
         var memState = e.target.parentElement.previousElementSibling.firstElementChild.value;
         var memNO = e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML;
         var xhr = new XMLHttpRequest();
         xhr.onload = function () {
             if (xhr.status == 200) {
                // alert("修改成功");
                // location.reload();   
                Swal.fire({
                html: '<div style="font-size:18px">修改成功!!!</div>',
                    confirmButtonText: '確定',
                    confirmButtonColor: '#f46f65',
                    background: '#fff url("./img/p0080_m.jpg") center'
                }).then(result => {
                    if (result.value) {
                        location.reload();
                    }
                })
             } else {
                 alert(xhr.status);
             }
         }
     
         var url = `./php/mem/membackaupdatemem.php?mem_State=${memState}&mem_NO=${memNO}`;
         xhr.open("Get", url, true);
         xhr.send(null);
     });
    }
   
} 

function getmemdata() {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmemdata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memback.php";
    xhr.open("Get", url, true);
    xhr.send(null);
}
window.addEventListener("load",doFirst);
