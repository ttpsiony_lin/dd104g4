<?php

// require_once("connectBooksACT.php");
require_once("../connectBook.php");

$pdo->beginTransaction();


if ($_FILES["uploadActRefImg"]["error"] == UPLOAD_ERR_OK) {

    $sql = "INSERT INTO `activity` VALUES (NULL, :mem_NO, :actTitle, NULL, :actStatement, 0, '', :limitNo_Competitor, NULL )";
    $activity = $pdo->prepare($sql);
    $activity->bindValue(':mem_NO', $_POST['mem_NO']);
    $activity->bindValue(':actTitle', $_POST['actTitle']);
    $activity->bindValue(':actStatement', $_POST['actStatement']);
    $activity->bindValue(':limitNo_Competitor', $_POST['limitNo_Competitor']);
    $activity->execute();

    //取得自動創號的key值
    $activity_NO = $pdo->lastInsertId();

    //將檔案copy到要放的路徑
    $fileInfoArr = pathinfo($_FILES["uploadActRefImg"]["name"]);
    $fileName = "{$activity_NO}_{$fileInfoArr["basename"]}";
    $from = $_FILES['uploadActRefImg']['tmp_name'];
    $to = "est_images/" . $fileName;

    if (copy($from, $to)) {
        //將檔案名稱寫回資料庫
        $sql = "update `activity` set activity_Pic = :activity_Pic where activity_NO = {$activity_NO}";
        $activity = $pdo->prepare($sql);
        $activity->bindValue(':activity_Pic', "est_images/" . $fileName);
        $activity->execute();
        echo "上傳成功";
        $pdo->commit();
    } else {
        echo "上傳失敗";
        $pdo->rollBack();
    }
} else {
    echo "上傳失敗";
    $pdo->rollBack();
}
