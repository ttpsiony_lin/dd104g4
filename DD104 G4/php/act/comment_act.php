<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");


$commentDate = json_decode(file_get_contents("php://input"));

if ($commentDate->comment_no == -1) {

    $mem_NO = $commentDate->mem_NO;
    $registration_NO = $commentDate->register_no;
    $comment_content = $commentDate->comment_content;

    $sql = "INSERT INTO `comment` VALUES(NULL, :mem_NO, :date, :comment_content, 1, :registration_NO)";
    $comment = $pdo->prepare($sql);

    $comment->bindValue(":mem_NO", $mem_NO);
    $comment->bindValue(":date", date("Y-m-d"));
    $comment->bindValue(":comment_content", $comment_content);
    $comment->bindValue(":registration_NO", $registration_NO);
    $comment->execute();

    $commentID = $pdo->lastInsertId();

    $sql = 'select mem_Pic from `member` where mem_NO = :mem_NO';
    $memSearchPic = $pdo->prepare($sql);
    $memSearchPic->bindValue(":mem_NO", $mem_NO);
    $memSearchPic->execute();
    $memPic = $memSearchPic->fetchObject();
    $memPic = $memPic->mem_Pic;
    echo json_encode([$commentID,  $memPic]);

    // 
} else {
    $msgNO = $commentDate->comment_no;
    $sql = "update `comment` set comment_state = 2 where comment_NO = {$msgNO}";
    $pdo->exec($sql);
    echo $commentDate->comment_no;
}
