<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");



if ($_POST["isChange"] == 0) {

    $sql = "select comment_NO, mem_NO, comment_content, comment_state from `comment` where comment_state in (0, 2)";
    $comment = $pdo->query($sql);
    $data = array();
    while ($Row = $comment->fetchObject()) {
        $data[] = $Row;
    }
    echo json_encode($data);
} else {

    $sql = "update `comment` set comment_state = :comment_state where comment_NO = :comment_NO";
    $state = $pdo->prepare($sql);
    $state->bindValue(":comment_state", $_POST["comment_state"]);
    $state->bindValue(":comment_NO", $_POST["comment_NO"]);
    $state->execute();

    echo json_encode(["status" => "ok"]);
}
